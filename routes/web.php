<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@front')->name('front');

Route::group(['prefix' => 'api'], function () {
    Route::resource('/hotdogs', 'HotDogController')->except(['show', 'create', 'edit']);
});

