require('./bootstrap');

$(function() {
    'use strict';

    const hotdog_container = $('#hotdog-container');
    const form = $('.create-edit-form');
    const delete_form = $('.delete-form');
    const title_input = $("input[name='title']", form);
    const image_number_input = $("select[name='image_number']", form);
    const id_input = $("input[name='id']");
    const submit_btn = $(".submit-btn", form);
    const delete_btn = $(".submit-btn", delete_form);
    let selected_hotdog;

    // counter
    const counter_el = $('#hotdog-counter');
    let counter = 0;

    const renderCounter = () => {
        counter_el.html(counter);
    };


    const hotdog_tmpl = _.template($('#hotdog-template').html());

    // generate image url to storage based on id of image
    function getImageUrl(id) {
        return document.location.origin + '/storage/images/hotdog-' + id + '.jpg';
    }

    // append hotdog to the container
    function renderHotdog(response) {
        const data = {
            id: response.id,
            title: response.title,
            image: getImageUrl(response.image_number),
            number: response.image_number,
        };
        return hotdog_tmpl(data);
    }

    // document rdy, get all existing hotdogs and render
    $.ajax({
        url: document.location.origin + '/api/hotdogs',
        type: 'GET',
        headers: {
            'X-CSRF-TOKEN': window.token.content
        },
        dataType: 'json',
        async: true,
        error: function() {
            console.log('Can\'t get hotdogs');
        },
        success: function(response) {
            $.each(response.data, (key, value) => {
                counter = counter + 1;
                renderCounter();
                hotdog_container.append(renderHotdog(value));
            });
        }
    });

    form.on('submit', function(e) {
        e.preventDefault();

        const title = title_input.val();
        const image_number = image_number_input.val();

        if (selected_hotdog) {
            const id = id_input.val();
            $.ajax({
                url: document.location.origin + '/api/hotdogs/' + id,
                data: {
                    '_method': 'PUT',
                    'title': title,
                    'image_number': image_number
                },
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': window.token.content
                },
                dataType: 'json',
                async: true,
                error: function() {
                    console.log('Can\'t update a hotdog');
                },
                success: function(response) {
                    selected_hotdog.wrap("<div class='temporary'></div>");
                    let updated = renderHotdog(response);
                    let temporary = $(".temporary");

                    temporary.html(updated);

                    selected_hotdog = temporary.find('.hotdog-instance');
                    selected_hotdog.unwrap();
                    selected_hotdog.addClass('selected');
                }
            });
        } else {
            $.ajax({
                url: document.location.origin + '/api/hotdogs',
                data: {
                    'title': title,
                    'image_number': image_number
                },
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': window.token.content
                },
                dataType: 'json',
                async: true,
                error: function() {
                    console.log('Can\'t create a hotdog');
                },
                success: function(response) {
                    hotdog_container.append(renderHotdog(response));
                    counter = counter + 1;
                    renderCounter();
                }
            });
        }
    });

    // select hotdog delegation
    hotdog_container.click(function(e) {
        let new_selected = $(e.target).closest('.hotdog-instance');
        if (new_selected.length) {

            if (new_selected.is(selected_hotdog)) {
                selected_hotdog.removeClass('selected');
                submit_btn.text('Create');
                selected_hotdog = null;
                delete_btn.prop('disabled', true);
                return;
            }

            if (selected_hotdog) {
                selected_hotdog.removeClass('selected');
            }

            selected_hotdog = new_selected;

            if (selected_hotdog) {
                selected_hotdog.addClass('selected');
                submit_btn.text('Update');
                delete_btn.prop('disabled', false);
            }
        } else {
            if (selected_hotdog) {
                selected_hotdog.removeClass('selected');
                submit_btn.text('Create');
                delete_btn.prop('disabled', true);
                selected_hotdog = null;
            }
        }

        // set form input values as selected hotdog
        if (selected_hotdog) {
            title_input.val(selected_hotdog.find('h6').text());
            image_number_input.val(selected_hotdog.find('img').data('number'));
            id_input.val(selected_hotdog.data('id'));
        }
    });

    delete_form.on('submit', function(e) {
        e.preventDefault();

        const id = id_input.val();
        $.ajax({
            url: document.location.origin + '/api/hotdogs/' + id,
            data: {
                '_method': 'DELETE',
            },
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': window.token.content
            },
            dataType: 'json',
            async: true,
            error: function() {
                console.log('Can\'t delete a hotdog');
            },
            success: function() {
                if (selected_hotdog) {
                    selected_hotdog.remove();
                    submit_btn.text('Create');
                    delete_btn.prop('disabled', true);
                    selected_hotdog = null;
                    counter = counter - 1;
                    renderCounter();
                }
            }
        });
    });

});
