<script type="text/template" id="hotdog-template">
    <div class='hotdog-instance' data-id='<%= id %>'>
        <h6><strong><%= title %></strong></h6>
        <div>
           <img src="<%= image %>" alt="HotDog" width="150" height="150" data-number="<%= number %>">
        </div>
    </div>
</script>