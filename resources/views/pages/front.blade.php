@extends('layouts.main')
@section('title', 'Hot Dogs | REST API')

@section('content')
    <div class="container">
        <h1 class="text-center my-3 text-info">Hot dogs</h1>
        <div class="row">
            <div class="col-12 col-lg-11">
                <form method="POST" action="{{ route('hotdogs.store') }}" class="create-edit-form">
                    <input type="hidden" name="id" value="">
                    <div class="form-row align-items-end">
                        <div class="col-sm-5 my-1">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" id="title" name="title"
                                   placeholder="Hotdog title" required maxlength="80">
                        </div>
                        <div class="col-sm-5 my-1">
                            <label class="mr-sm-2" for="image_selector">Image</label>
                            <select class="custom-select mr-sm-2" id="image_selector" name="image_number" required>
                                <option selected value="1">Image #1</option>
                                <option value="2">Image #2</option>
                                <option value="3">Image #3</option>
                            </select>
                        </div>
                        <div class="col-12 col-sm-auto my-1 submit">
                            <button type="submit" class="btn btn-primary submit-btn">Create</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-12 col-lg-1 d-flex align-items-end">
                <form method="POST" action="{{ route('hotdogs.destroy', 0) }}" class="delete-form">
                    <input type="hidden" name="id" value="">
                    <div class="form-row align-items-end">
                        <div class="col-12 my-1">
                            <button type="submit" class="btn btn-danger submit-btn" disabled>Delete</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-12">
                <hr>
                <div class="d-flex justify-content-between text-info">
                    <span>Click to select a hot dog</span>
                    <span>Total: <span id="hotdog-counter">0</span></span>
                </div>
            </div>

            <div class="col-12">
               <div id="hotdog-container" class="d-flex justify-content-center align-items-center flex-wrap">
               </div>
            </div>
        </div>
    </div>
@endsection