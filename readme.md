# Hot Dog's REST API

Test task to make Hot dog's REST API with CRUD demonstration using ajax.

Demonstration: [http://vps29009ua.hyperhost.name]

### Used

*  Backend: [Laravel] (php)
*  Frontend: js(JQuery), lodash(template)
*  DB: MySQL
*  Styling: Bootstrap, SCSS
*  Deployment: VPS, CentOS 7, Apache

### Installation

```sh
$ git clone
  copy .env
$ composer install
$ php artisan key:generate
$ php artisan storage:link
  setup DB in .env
$ php artisan migrate
$ npm install
$ npm run dev 
```

Dev by [EXayer].


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [http://vps29009ua.hyperhost.name]: <http://vps29009ua.hyperhost.name/>
   [Laravel]: <https://github.com/laravel/framework>
   [EXayer]: <mailto:itwasntluck@gmail.com>
