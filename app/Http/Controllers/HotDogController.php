<?php

namespace App\Http\Controllers;

use App\Http\Resources\Hotdog as HotDogResource;
use App\Models\Hotdog;
use Illuminate\Http\Request;

class HotDogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return HotDogResource::collection(Hotdog::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hotdog = Hotdog::create($request->all());

        return response()->json($hotdog, 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Hotdog $hotdog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hotdog $hotdog)
    {
        $hotdog->update($request->all());

        return response()->json($hotdog, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Hotdog $hotdog
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Hotdog $hotdog)
    {
        $hotdog->delete();

        return response()->json(null, 204);
    }
}
